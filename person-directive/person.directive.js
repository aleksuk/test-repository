(function () {	

	angular.module('MyApp')
		.directive('myPerson', function () {
			return {
				restrict: 'E',
				scope: {},
				bindToController: {
					person: '=',
					personName: '='
				},
				controller: PersonController,
				controllerAs: 'personController',
				templateUrl: 'person-directive/person.html'
			}
		});

		function PersonController($http, $stateParams) {
			var vm = this;

			$http.get('/person/' + $stateParams.id)
				.then(function (res) {
					vm.user = res.data;
				});
		}

} ());