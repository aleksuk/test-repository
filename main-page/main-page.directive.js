(function () {

	angular.module('MyApp')
		.directive('mainPage', function () {
			return {
				restrict: 'E',
				templateUrl: '/main-page/main-page.html',
			};
		});

} ());