(function () {

	angular
		.module('MyApp', [
			'ui.router'
		])
		.config(function ($stateProvider, $urlRouterProvider) {
			$stateProvider.state('main', {
				url: '/main-page',
				template: '<main-page></main-page>'
			});

			$stateProvider.state('main.users', {
				url: '/users',
				template: '<another-page></another-page>;'
			});

			$stateProvider.state('main.user', {
				url: '/user/:id',
				template: '<my-person></my-person>;'
			});

			// $urlRouterProvider.otherwise('/');
		})
		.run(function () {
			console.log('run');
		});

} ());
