(function () {

	angular
		.module('MyApp')
		.constant('PERSON_URL', '/person')
		.factory('personFactory', personFactory);

	function personFactory($http, PERSON_URL) {
		return {
			get: function () {
				return $http.get(PERSON_URL)
					.then(function (res) {
						return res.data;
					});
			},
		};
	}

	personFactory.$inject = [ '$http', 'PERSON_URL' ];

} ());