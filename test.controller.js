(function () {

	angular.module('MyApp')
		.controller('TestController', function (personFactory, $scope) {
			var vm = this;
			// console.log($scope);
			vm.edit = edit;
			vm.showModal = showModal;

			activate();

			function activate() {
				vm.person = null;

				personFactory.get()
					.then(function (data) {
						vm.persons = data;
					});
			}

			function edit(person) {
				console.log(person);
			}

			function showModal() {
				// ..
			}
		});

} ());
