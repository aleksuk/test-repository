(function () {

	angular.module('MyApp')
		.directive('anotherPage', function () {
			return {
				restrict: 'E',
				templateUrl: 'another-page/another-page.html',
				controller: function ($http) {
					var vm = this;

					$http.get('/person')
						.then(function (res) {
							console.log(res);
							vm.users = res.data
						});
				},
				controllerAs: '$ctrl',
				scope: {}
			};
		});

} ());